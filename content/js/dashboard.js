/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.8943782735931705, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.6085365853658536, 500, 1500, "getCountCheckInOutChildren"], "isController": false}, {"data": [0.9999518258021004, 500, 1500, "getLatestMobileVersion"], "isController": false}, {"data": [0.9443819170887263, 500, 1500, "findAllConfigByCategory"], "isController": false}, {"data": [0.6408991228070176, 500, 1500, "getNotifications"], "isController": false}, {"data": [0.12262038073908174, 500, 1500, "getHomefeed"], "isController": false}, {"data": [0.799422554347826, 500, 1500, "me"], "isController": false}, {"data": [0.7922636103151862, 500, 1500, "findAllChildrenByParent"], "isController": false}, {"data": [0.37241948802642444, 500, 1500, "getAllClassInfo"], "isController": false}, {"data": [0.916648315349042, 500, 1500, "findAllSchoolConfig"], "isController": false}, {"data": [0.11609071274298056, 500, 1500, "getChildCheckInCheckOut"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 53649, 0, 0.0, 279.0085556114751, 10, 2978, 59.0, 846.9000000000015, 1345.9500000000007, 1814.0, 178.32356108651427, 749.0367233359343, 210.75431257000952], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["getCountCheckInOutChildren", 2050, 0, 0.0, 729.6131707317074, 224, 1927, 693.0, 1100.0, 1198.4499999999998, 1430.45, 6.832285716190184, 4.316883650756884, 8.45361914298141], "isController": false}, {"data": ["getLatestMobileVersion", 31137, 0, 0.0, 47.74323152519502, 10, 1388, 32.0, 86.0, 125.0, 215.0, 103.83845794704196, 69.46224970090208, 76.56058178712566], "isController": false}, {"data": ["findAllConfigByCategory", 5331, 0, 0.0, 280.52072781842014, 41, 1398, 242.0, 518.8000000000002, 624.0, 895.0, 17.745924695496445, 20.068145309946175, 23.048906098642842], "isController": false}, {"data": ["getNotifications", 1824, 0, 0.0, 822.0317982456143, 80, 1820, 817.5, 1398.0, 1517.75, 1668.25, 6.065180741721061, 50.42273794362441, 6.7463289695510635], "isController": false}, {"data": ["getHomefeed", 893, 0, 0.0, 1678.729003359465, 643, 2978, 1658.0, 2042.2, 2312.6, 2667.079999999999, 2.9694212787465255, 36.18112235832069, 14.858705695602728], "isController": false}, {"data": ["me", 2944, 0, 0.0, 509.083559782608, 74, 1537, 431.0, 957.0, 1077.75, 1276.6500000000005, 9.79224736068333, 12.880925183812524, 31.03109637247793], "isController": false}, {"data": ["findAllChildrenByParent", 2792, 0, 0.0, 536.6436246418334, 69, 1545, 426.5, 1082.1000000000008, 1185.0, 1373.0700000000002, 9.287101838792942, 10.429850697863168, 14.837596297134038], "isController": false}, {"data": ["getAllClassInfo", 1211, 0, 0.0, 1237.3798513625104, 135, 2285, 1411.0, 1728.0, 1821.199999999999, 1971.5599999999986, 4.027564371187782, 11.556642404831747, 10.340299542824882], "isController": false}, {"data": ["findAllSchoolConfig", 4541, 0, 0.0, 329.7104162078835, 42, 1502, 284.0, 604.0, 726.0, 1012.5799999999999, 15.10595123249393, 329.4395536367719, 11.078681030862246], "isController": false}, {"data": ["getChildCheckInCheckOut", 926, 0, 0.0, 1619.3974082073428, 593, 2769, 1627.0, 1872.3000000000002, 1956.3, 2136.73, 3.077945820176167, 204.8727628490111, 14.163360063154395], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 53649, 0, null, null, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
